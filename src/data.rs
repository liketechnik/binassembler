// SPDX-FileCopyrightText: 2021 Florian Warzecha <florian_filip.warzecha@fh-bielfeld.de
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use bitvec::prelude::*;

pub type InstructionBits = BitArr!(for 32, in Lsb0, u32);

pub struct RawInstruction(pub InstructionBits);

pub trait InstructionBitsAccessor {
    fn instruction_bits(&self) -> &BitSlice<Lsb0, u32>;
}

impl InstructionBitsAccessor for RawInstruction {
    fn instruction_bits(&self) -> &BitSlice<Lsb0, u32> {
        self.0.as_bitslice()
    }
}

pub trait Funct7: InstructionBitsAccessor {
    fn funct7(&self) -> u8 {
        self.instruction_bits()[25..32].load::<u8>()
    }
}

pub trait Rs2: InstructionBitsAccessor {
    fn rs2(&self) -> u8 {
        self.instruction_bits()[20..25].load::<u8>()
    }
}

pub trait Rs1: InstructionBitsAccessor {
    fn rs1(&self) -> u8 {
        self.instruction_bits()[15..20].load::<u8>()
    }
}

pub trait Funct3: InstructionBitsAccessor {
    fn funct3(&self) -> u8 {
        self.instruction_bits()[12..15].load::<u8>()
    }
}

pub trait Rd: InstructionBitsAccessor {
    fn rd(&self) -> u8 {
        self.instruction_bits()[7..12].load::<u8>()
    }
}

pub trait Opcode: InstructionBitsAccessor {
    fn opcode(&self) -> u8 {
        self.instruction_bits()[0..7].load::<u8>()
    }
}

pub trait ImmI: InstructionBitsAccessor {
    fn imm_i_raw(&self) -> u16 {
        self.instruction_bits()[20..32].load::<u16>()
    }

    fn imm_i(&self) -> i32 {
        let mut bits = InstructionBits::zeroed();

        bits[0..11].copy_from_bitslice(&self.instruction_bits()[20..31]);
        bits[11..32].for_each(|_, _| self.instruction_bits()[31]);

        bits[0..32].load::<u32>() as i32
    }
}

pub trait ImmS: InstructionBitsAccessor {
    fn imm_s_raw_lower(&self) -> u8 {
        self.instruction_bits()[7..12].load::<u8>()
    }

    fn imm_s_raw_upper(&self) -> u8 {
        self.instruction_bits()[25..32].load::<u8>()
    }

    fn imm_s(&self) -> i32 {
        let mut bits = InstructionBits::zeroed();

        bits[0..5].copy_from_bitslice(&self.instruction_bits()[7..12]);
        bits[5..11].copy_from_bitslice(&self.instruction_bits()[25..31]);
        bits[11..32].for_each(|_, _| self.instruction_bits()[31]);

        bits[0..32].load::<u32>() as i32
    }
}

pub trait ImmB: InstructionBitsAccessor {
    fn imm_b_raw_1_4(&self) -> u8 {
        self.instruction_bits()[8..12].load::<u8>()
    }

    fn imm_b_raw_5_10(&self) -> u8 {
        self.instruction_bits()[25..31].load::<u8>()
    }

    fn imm_b_raw_11(&self) -> u8 {
        self.instruction_bits()[7..8].load::<u8>()
    }

    fn imm_b_raw_12(&self) -> u8 {
        self.instruction_bits()[31..32].load::<u8>()
    }

    fn imm_b(&self) -> i32 {
        let mut bits = InstructionBits::zeroed();

        bits[1..5].copy_from_bitslice(&self.instruction_bits()[8..12]);
        bits[5..11].copy_from_bitslice(&self.instruction_bits()[25..31]);
        bits[11..12].copy_from_bitslice(&self.instruction_bits()[7..8]);
        bits[12..32].for_each(|_, _| self.instruction_bits()[31]);

        bits[0..32].load::<u32>() as i32
    }
}

pub trait ImmU: InstructionBitsAccessor {
    fn imm_u_raw(&self) -> u32 {
        self.instruction_bits()[12..32].load::<u32>()
    }

    fn imm_u(&self) -> i32 {
        // we can 'ignore' sign extension here
        // because the sign bit from the instruction
        // is already loaded as the highest bit
        let val = self.instruction_bits()[12..32].load::<u32>();
        (val << 12) as i32
    }
}

pub trait ImmJ: InstructionBitsAccessor {
    fn imm_j_raw_1_10(&self) -> u16 {
        self.instruction_bits()[21..32].load::<u16>()
    }

    fn imm_j_raw_11(&self) -> u8 {
        self.instruction_bits()[20..21].load::<u8>()
    }

    fn imm_j_raw_12_19(&self) -> u8 {
        self.instruction_bits()[12..20].load::<u8>()
    }

    fn imm_j_raw_20(&self) -> u8 {
        self.instruction_bits()[20..21].load::<u8>()
    }

    fn imm_j(&self) -> i32 {
        let mut bits = InstructionBits::zeroed();

        bits[1..11].copy_from_bitslice(&self.instruction_bits()[21..31]);
        bits[11..12].copy_from_bitslice(&self.instruction_bits()[20..21]);
        bits[12..20].copy_from_bitslice(&self.instruction_bits()[12..20]);
        bits[20..32].for_each(|_, _| self.instruction_bits()[31]);

        bits[0..32].load::<u32>() as i32
    }
}

impl Funct7 for RawInstruction {}
impl Rs2 for RawInstruction {}
impl Rs1 for RawInstruction {}
impl Funct3 for RawInstruction {}
impl Rd for RawInstruction {}
impl ImmI for RawInstruction {}
impl ImmS for RawInstruction {}
impl ImmB for RawInstruction {}
impl ImmU for RawInstruction {}
impl ImmJ for RawInstruction {}

impl<T> Opcode for T where T: InstructionBitsAccessor {}

pub enum DecodedInstruction {
    R(RTypeInstruction),
    I(ITypeInstruction),
    S(STypeInstruction),
    B(BTypeInstruction),
    U(UTypeInstruction),
    J(JTypeInstruction),
    Unknown(RawInstruction),
}

impl DecodedInstruction {
    pub fn name(&self) -> Option<&'static str> {
        match self {
            DecodedInstruction::R(i) => match i.opcode() {
                0b0110011 => match i.funct7() {
                    0 => match i.funct3() {
                        0b000 => Some("add"),
                        0b001 => Some("sll"),
                        0b010 => Some("slt"),
                        0b011 => Some("sltu"),
                        0b100 => Some("xor"),
                        0b101 => Some("srl"),
                        0b110 => Some("or"),
                        0b111 => Some("and"),
                        _ => None,
                    },
                    0b0100000 => match i.funct3() {
                        0b000 => Some("sub"),
                        0b101 => Some("sra"),
                        _ => None,
                    },
                    _ => None,
                },
                _ => {
                    eprintln!("Encountered unexpected opcode during name evaluation of r type instruction!");
                    None
                }
            },
            DecodedInstruction::I(i) => match i.opcode() {
                0b1100111 => Some("jalr"),
                0b0001111 => Some("fence"),
                0b1110011 => match i.imm_i() {
                    0b0 => Some("ecall"),
                    0b1 => Some("ebreak"),
                    _ => None,
                },
                0b0010011 => match i.funct3() {
                    0b0 => Some("addi"),
                    0b010 => Some("slti"),
                    0b011 => Some("sltiu"),
                    0b100 => Some("xori"),
                    0b110 => Some("ori"),
                    0b111 => Some("andi"),
                    0b001 => Some("slli"),
                    0b101 => {
                        let as_r_type = RTypeInstruction(i.0);
                        match as_r_type.funct7() {
                            0b0 => Some("srli"),
                            0b0100000 => Some("srai"),
                            _ => None,
                        }
                    }
                    _ => None,
                },
                0b0000011 => match i.funct3() {
                    0b0 => Some("lb"),
                    0b001 => Some("lh"),
                    0b010 => Some("lw"),
                    0b100 => Some("lbu"),
                    0b101 => Some("lhu"),
                    _ => None,
                },
                _ => {
                    eprintln!("Encountered unexpected opcode during name evaluation of r type instruction!");
                    None
                }
            },
            DecodedInstruction::S(i) => match i.opcode() {
                0b0100011 => match i.funct3() {
                    0b000 => Some("sb"),
                    0b001 => Some("sh"),
                    0b010 => Some("sw"),
                    _ => None,
                },
                _ => {
                    eprintln!("Encountered unexpected opcode during name evaluation of r type instruction!");
                    None
                }
            },
            DecodedInstruction::B(i) => match i.opcode() {
                0b1100011 => match i.funct3() {
                    0b000 => Some("beq"),
                    0b001 => Some("bne"),
                    0b100 => Some("blt"),
                    0b101 => Some("bge"),
                    0b110 => Some("bltu"),
                    0b111 => Some("bgeu"),
                    _ => None,
                },
                _ => {
                    eprintln!("Encountered unexpected opcode during name evaluation of r type instruction!");
                    None
                }
            },
            DecodedInstruction::U(i) => match i.opcode() {
                0b0110111 => Some("lui"),
                0b0010111 => Some("auipc"),
                _ => {
                    eprintln!("Encountered unexpected opcode during name evaluation of r type instruction!");
                    None
                }
            },
            DecodedInstruction::J(i) => match i.opcode() {
                0b1101111 => Some("jal"),
                _ => {
                    eprintln!("Encountered unexpected opcode during name evaluation of r type instruction!");
                    None
                }
            },
            DecodedInstruction::Unknown(_) => None,
        }
    }
}

impl From<RawInstruction> for DecodedInstruction {
    fn from(raw: RawInstruction) -> Self {
        match raw.opcode() {
            0b0110111 | 0b0010111 => DecodedInstruction::U(UTypeInstruction(raw.0)),
            0b1101111 => DecodedInstruction::J(JTypeInstruction(raw.0)),
            0b1100111 | 0b0000011 | 0b0001111 | 0b1110011 => {
                DecodedInstruction::I(ITypeInstruction(raw.0))
            }
            0b1100011 => DecodedInstruction::B(BTypeInstruction(raw.0)),
            0b0100011 => DecodedInstruction::S(STypeInstruction(raw.0)),
            0b0010011 => match raw.funct3() {
                0b000 | 0b010 | 0b011 | 0b100 | 0b110 | 0b111 | 0b001 | 0b101 => {
                    DecodedInstruction::I(ITypeInstruction(raw.0))
                }
                _ => DecodedInstruction::Unknown(RawInstruction(raw.0)),
            },
            0b0110011 => DecodedInstruction::R(RTypeInstruction(raw.0)),
            _ => DecodedInstruction::Unknown(RawInstruction(raw.0)),
        }
    }
}

pub struct RTypeInstruction(InstructionBits);

impl InstructionBitsAccessor for RTypeInstruction {
    fn instruction_bits(&self) -> &BitSlice<Lsb0, u32> {
        self.0.as_bitslice()
    }
}

impl Funct7 for RTypeInstruction {}
impl Rs2 for RTypeInstruction {}
impl Rs1 for RTypeInstruction {}
impl Funct3 for RTypeInstruction {}
impl Rd for RTypeInstruction {}

pub struct ITypeInstruction(InstructionBits);

impl InstructionBitsAccessor for ITypeInstruction {
    fn instruction_bits(&self) -> &BitSlice<Lsb0, u32> {
        self.0.as_bitslice()
    }
}

impl ImmI for ITypeInstruction {}
impl Rs1 for ITypeInstruction {}
impl Funct3 for ITypeInstruction {}
impl Rd for ITypeInstruction {}

pub struct STypeInstruction(InstructionBits);

impl InstructionBitsAccessor for STypeInstruction {
    fn instruction_bits(&self) -> &BitSlice<Lsb0, u32> {
        self.0.as_bitslice()
    }
}

impl ImmS for STypeInstruction {}
impl Rs2 for STypeInstruction {}
impl Rs1 for STypeInstruction {}
impl Funct3 for STypeInstruction {}

pub struct BTypeInstruction(InstructionBits);

impl InstructionBitsAccessor for BTypeInstruction {
    fn instruction_bits(&self) -> &BitSlice<Lsb0, u32> {
        self.0.as_bitslice()
    }
}

impl ImmB for BTypeInstruction {}
impl Rs2 for BTypeInstruction {}
impl Rs1 for BTypeInstruction {}
impl Funct3 for BTypeInstruction {}

pub struct UTypeInstruction(InstructionBits);

impl InstructionBitsAccessor for UTypeInstruction {
    fn instruction_bits(&self) -> &BitSlice<Lsb0, u32> {
        self.0.as_bitslice()
    }
}

impl ImmU for UTypeInstruction {}
impl Rd for UTypeInstruction {}

pub struct JTypeInstruction(InstructionBits);

impl InstructionBitsAccessor for JTypeInstruction {
    fn instruction_bits(&self) -> &BitSlice<Lsb0, u32> {
        self.0.as_bitslice()
    }
}

impl ImmJ for JTypeInstruction {}
impl Rd for JTypeInstruction {}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn simple() {
        let bitvec: BitArr!(for 32, in Lsb0, u32) =
            BitArray::<Lsb0, [u32; 1]>::new([u32::from_le(0x00730433)]);
        let raw = RawInstruction(bitvec);
        let decoded: DecodedInstruction = raw.into();
        match decoded {
            DecodedInstruction::R(i) => {
                assert_eq!(i.rd(), 8);
                assert_eq!(i.rs1(), 6);
                assert_eq!(i.rs2(), 7);
            }
            _ => unreachable!(),
        }
    }

    #[test]
    fn names() {
        let bitvec: InstructionBits =
            InstructionBits::new([u32::from_le(0b00000000011100100100000100110011)]);
        let raw = RawInstruction(bitvec);
        let decoded: DecodedInstruction = raw.into();
        match &decoded {
            DecodedInstruction::R(i) => {
                assert_eq!(decoded.name(), Some("xor"));
                assert_eq!(i.rs1(), 4);
                assert_eq!(i.rs2(), 7);
                assert_eq!(i.rd(), 2);
            }
            _ => unreachable!(),
        }

        let bitvec: InstructionBits = InstructionBits::new([u32::from_le(0x10000517)]);
        let raw = RawInstruction(bitvec);
        let decoded: DecodedInstruction = raw.into();
        match &decoded {
            DecodedInstruction::U(i) => {
                assert_eq!(decoded.name(), Some("auipc"));
                assert_eq!(i.rd(), 10);
                assert_eq!(i.imm_u(), 0x10000 << 12);
            }
            _ => unreachable!(),
        }
    }

    #[test]
    fn names2() {
        let bitvec = InstructionBits::new([u32::from_le(0x018000ef)]);
        let raw = RawInstruction(bitvec);
        let decoded: DecodedInstruction = raw.into();
        match &decoded {
            DecodedInstruction::J(i) => {
                assert_eq!(decoded.name(), Some("jal"));
                assert_eq!(i.rd(), 1);
                let expected: u32 = 0x24 - 0xc; // fake pc
                let result: u32 = i.imm_j() as u32;
                assert_eq!(result, expected);
            }
            _ => unreachable!(),
        }
    }
}
