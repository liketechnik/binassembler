// SPDX-FileCopyrightText: 2021 Florian Warzecha <florian_filip.warzecha@fh-bielfeld.de
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use binassembler::data::{
    DecodedInstruction, Funct3, Funct7, ImmB, ImmI, ImmJ, ImmS, ImmU, InstructionBits, Opcode,
    RawInstruction, Rd, Rs1, Rs2,
};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    loop {
        if let Err(err) = repl() {
            eprintln!("Error: {:?}", err);
        }
    }
}

macro_rules! println_all_formats {
    ($text:expr, $accessor:expr, us) => {
        println!(
            "{} {} (unsigned), {:#x}, {:#o}, {:#b}",
            $text, $accessor, $accessor, $accessor, $accessor
        )
    };
    ($text:expr, $accessor:expr, si) => {
        println!(
            "{} {:+}, {:#x}, {:#o}, {:#b}",
            $text, $accessor, $accessor, $accessor, $accessor
        )
    };
    ($text:expr, $accessor:expr) => {
        println!(
            "{} {}, {:#x}, {:#o}, {:#b}",
            $text, $accessor, $accessor, $accessor, $accessor
        )
    };
}

fn repl() -> Result<(), Box<dyn Error>> {
    println!(
        "Please enter a machine code instruction (prefix with 0x, 0b, or 0o for a given base): "
    );
    let mut input = String::new();
    std::io::stdin().read_line(&mut input)?;
    let input = input.trim();

    let number = match &input[0..2] {
        "0x" => u32::from_str_radix(&input[2..], 16),
        "0b" => u32::from_str_radix(&input[2..], 2),
        "0o" => u32::from_str_radix(&input[2..], 8),
        _ => input.parse::<u32>(),
    }?;
    let number = u32::from_le(number);

    println!();

    let instruction_raw = RawInstruction(InstructionBits::new([number]));
    let decoded: DecodedInstruction = instruction_raw.into();
    match &decoded {
        DecodedInstruction::R(i) => {
            println!(
                "{} x{}, x{}, x{}",
                decoded.name().unwrap_or("unknown"),
                i.rd(),
                i.rs1(),
                i.rs2()
            );
            println!("Type: R");
            println_all_formats!("funct7:", i.funct7());
            println_all_formats!("rs2:", i.rs2());
            println_all_formats!("rs1:", i.rs1());
            println_all_formats!("funct3:", i.funct3());
            println_all_formats!("rd:", i.rd());
            println_all_formats!("opcode:", i.opcode());
        }
        DecodedInstruction::I(i) => {
            let imm = i.imm_i();
            println!(
                "{} x{}, x{}, {:+}",
                decoded.name().unwrap_or("unknown"),
                i.rd(),
                i.rs1(),
                imm
            );
            println_all_formats!("imm:", imm, si);
            println!("Type: I");
            println_all_formats!("rs1:", i.rs1());
            println_all_formats!("funct3:", i.funct3());
            println_all_formats!("rd:", i.rd());
            println_all_formats!("opcode:", i.opcode());
        }
        DecodedInstruction::S(i) => {
            let imm = i.imm_s();
            println!(
                "{} x{}, {:+}(x{})",
                decoded.name().unwrap_or("unknown"),
                i.rs2(),
                imm,
                i.rs1()
            );
            println_all_formats!("imm:", imm, si);
            println!("Type: S");
            println_all_formats!("imm [5:11]:", i.imm_s_raw_upper());
            println_all_formats!("rs2:", i.rs2());
            println_all_formats!("rs1:", i.rs1());
            println_all_formats!("funct3:", i.funct3());
            println_all_formats!("imm [0:4]:", i.imm_s_raw_lower());
            println_all_formats!("opcode:", i.opcode());
        }
        DecodedInstruction::B(i) => {
            let imm = i.imm_b();
            println!(
                "{} x{}, x{}, {:+}",
                decoded.name().unwrap_or("unknown"),
                i.rs1(),
                i.rs2(),
                imm
            );
            println_all_formats!("imm:", imm, si);
            println!("Type: B");
            println_all_formats!("imm [12]:", i.imm_b_raw_12());
            println_all_formats!("imm [5:10]:", i.imm_b_raw_5_10());
            println_all_formats!("rs2:", i.rs2());
            println_all_formats!("rs1:", i.rs1());
            println_all_formats!("funct3:", i.funct3());
            println_all_formats!("imm [11]:", i.imm_b_raw_11());
            println_all_formats!("imm [1:4]:", i.imm_b_raw_1_4());
            println_all_formats!("opcode:", i.opcode());
        }
        DecodedInstruction::U(i) => {
            let imm = i.imm_u();
            println!(
                "{} x{}, {:+}",
                decoded.name().unwrap_or("unknown"),
                i.rd(),
                imm
            );
            println_all_formats!("imm:", imm, si);
            println!("Type: U");
            println_all_formats!("imm [12:31]:", i.imm_u_raw());
            println_all_formats!("rd:", i.rd());
            println_all_formats!("opcode:", i.opcode());
        }
        DecodedInstruction::J(i) => {
            let imm = i.imm_j();
            println!(
                "{} x{}, {:+}",
                decoded.name().unwrap_or("unknown"),
                i.rd(),
                imm
            );
            println_all_formats!("imm:", imm, si);
            println!("Type: J");
            println_all_formats!("imm [20]:", i.imm_j_raw_20());
            println_all_formats!("imm [1:10]:", i.imm_j_raw_1_10());
            println_all_formats!("imm [11]:", i.imm_j_raw_11());
            println_all_formats!("imm [12:19]:", i.imm_j_raw_12_19());
            println_all_formats!("rd:", i.rd());
            println_all_formats!("opcode:", i.opcode());
        }
        DecodedInstruction::Unknown(i) => {
            println!("Unknown instruction encountered! Following data may help you decode the instruction, though it may not apply to the type of instruction currently inspected:");
            println_all_formats!("opcode:", i.opcode());
            println_all_formats!("funct3:", i.funct3());
            println_all_formats!("funct7:", i.funct7());
            println_all_formats!("rd:", i.rd());
            println_all_formats!("rs1:", i.rs1());
            println_all_formats!("rs2:", i.rs2());
            println_all_formats!("imm (I-Type):", i.imm_i(), si);
            println_all_formats!("imm (S-Type):", i.imm_s(), si);
            println_all_formats!("imm [5:11]:", i.imm_s_raw_upper());
            println_all_formats!("imm [0:4]:", i.imm_s_raw_lower());
            println_all_formats!("imm (B-Type):", i.imm_b(), si);
            println_all_formats!("imm [12]:", i.imm_b_raw_12());
            println_all_formats!("imm [5:10]:", i.imm_b_raw_5_10());
            println_all_formats!("imm [11]:", i.imm_b_raw_11());
            println_all_formats!("imm [1:4]:", i.imm_b_raw_1_4());
            println_all_formats!("imm (U-Type):", i.imm_u(), si);
            println_all_formats!("imm [12:31]:", i.imm_u_raw());
            println_all_formats!("imm (J-Type):", i.imm_j(), si);
            println_all_formats!("imm [20]:", i.imm_j_raw_20());
            println_all_formats!("imm [1:10]:", i.imm_j_raw_1_10());
            println_all_formats!("imm [11]:", i.imm_j_raw_11());
            println_all_formats!("imm [12:19]:", i.imm_j_raw_12_19());
        }
    }

    println!("\n");

    Ok(())
}
